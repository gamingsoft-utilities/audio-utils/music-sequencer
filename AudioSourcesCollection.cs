﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace GamingSoft.AudioUtils.MusicSequencer
{
    public class AudioSourcesCollection
    {
        AudioSourcesPool sourcesPool;
        AudioMixerGroup defaultMixerGroup;
        List<AudioSource> sources;
        int currentIndex;

        public void Init(AudioSourcesPool sourcesPool, AudioMixerGroup defaultMixerGroup)
        {
            this.sourcesPool = sourcesPool;
            this.defaultMixerGroup = defaultMixerGroup;
            sources = new List<AudioSource>();
        }

        public AudioSource GetNextFree()
        {
            AudioSource toReturn = null;
            for (int i = 0; i < sources.Count; ++i)
            {
                if (sources[i].isPlaying) { continue; }
                toReturn = sources[i];
                currentIndex = i;
                break;
            }

            if (toReturn == null)
            {
                sources.Add(sourcesPool.Get());
                currentIndex = sources.Count - 1;
                sources[currentIndex].outputAudioMixerGroup = defaultMixerGroup;
                toReturn = sources[currentIndex];
            }

            toReturn.volume = 1; // set default volume
            return toReturn;
        }

        public AudioSource GetCurent()
        {
            return sources.Count > currentIndex ? sources[currentIndex] : null;
        }

        public List<AudioSource> GetAllActive()
        {
            return sources.FindAll(w => w.isPlaying);
        }

        public void Release(AudioSource source)
        {
            sources.Remove(source);
            currentIndex = Mathf.Max(sources.Count - 1,0);
            sourcesPool.Return(source);
        }
    }
}