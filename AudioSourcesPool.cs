﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamingSoft.AudioUtils.MusicSequencer
{
    public class AudioSourcesPool
    {
        GameObject container;
        Queue<AudioSource> sources;

        public AudioSourcesPool(GameObject container)
        {
            this.container = container;
            sources = new Queue<AudioSource>();
        }

        public AudioSource Get()
        {
            if (sources.Count > 0)
            {
                return sources.Dequeue();
            }
            return container.AddComponent<AudioSource>();
        }

        public void Return(AudioSource source)
        {
            if (source.isPlaying) { source.Stop(); }
            source.clip = null;
            sources.Enqueue(source);
        }
    }
}
