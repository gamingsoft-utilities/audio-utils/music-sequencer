﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamingSoft.AudioUtils.MusicSequencer 
{
    [CreateAssetMenu(
    fileName = "NewMusicPhase",
    menuName = "Gamingsoft/AudioUtils/MusicPhase")]
    public class MusicPhaseAsset : ScriptableObject
    {
        public AudioClip audioClip;
        public int bpm;
        public BarType barType;
        public Vector2 loopStartEndBar;

        private double _clipLength = -1; // cache audio clip length

        public double Bars { get; private set; }
        public double Beats { get; private set; }
        public double BeatsOffset { get; private set; }

        public double SecondsPerBeat { get; private set; }
        public double SecondsPerBar { get; private set; }

        public double Length
        {
            get
            {
                if (audioClip == null){ _clipLength = -1; } // invalidate cached length
				
				if (audioClip != null && _clipLength == -1)
                {
                    _clipLength = (double)audioClip.samples / audioClip.frequency;
                }
                return _clipLength;
            }
        }
        public double LoopLength => SecondsPerBar * (loopStartEndBar.y - loopStartEndBar.x);

        public bool IsValid => audioClip != null && bpm > 0;

        public void CalculateStats() 
        {
            if (!IsValid) { return; } // do nothing until all parameters are valid

            int beatsPerBar = 0;
            switch(barType)
            {
                case BarType.FourByFour:
                    beatsPerBar = 4;
                    break;
                case BarType.SixByEight:
                    beatsPerBar = 3;
                    break;
            }

            SecondsPerBeat = (60f / bpm);

            SecondsPerBar = SecondsPerBeat * beatsPerBar;
            Beats = Length / SecondsPerBeat;
            Bars = Beats / beatsPerBar;
            BeatsOffset = System.Math.Floor((Bars - System.Math.Truncate(Bars)) * beatsPerBar);
        }
    }
}
