﻿using UnityEngine;
using UnityEditor;

namespace GamingSoft.AudioUtils.MusicSequencer
{
    [CustomEditor(typeof(MusicSequencer))]
    public class MusicSequencerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            MusicSequencer musicSequencer = target as MusicSequencer;

            EditorGUIUtility.labelWidth = 50;

            EditorGUILayout.BeginHorizontal("box");

            EditorGUILayout.LabelField("Phase: ", (musicSequencer.CurrentPhaseIndex + 1).ToString());
            if (GUILayout.Button("To Previous Phase")) { musicSequencer.ToPreviousPhase(); }
            if (GUILayout.Button("To Next Phase")) { musicSequencer.ToNextPhase(); }
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal("box");
            if (GUILayout.Button("Play")) { musicSequencer.Play(); }
            if (GUILayout.Button("Pause")) { musicSequencer.Pause(); }
            if (GUILayout.Button("Resume")) { musicSequencer.Resume(); }
            if (GUILayout.Button("Stop")) { musicSequencer.Stop(); }
            if (GUILayout.Button ("Stop All!")) { musicSequencer.StopAll (); }
            EditorGUILayout.EndHorizontal();
        }
    }
}