﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace GamingSoft.AudioUtils.MusicSequencer 
{
    [CustomEditor(typeof(MusicPhaseAsset))]
    public class MusicPhaseAssetEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            MusicPhaseAsset phase = target as MusicPhaseAsset;
            phase.CalculateStats();

            EditorGUILayout.Space();

            EditorGUILayout.LabelField("Length (s)", phase.Length.ToString("#.00"));
            EditorGUILayout.LabelField("Length (beats)", ((int)phase.Beats).ToString());
            EditorGUILayout.LabelField("Length (bars)", string.Format("{0} bars and {1} beats", (int)phase.Bars, (int)phase.BeatsOffset));

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Loop Length (s)",phase.LoopLength.ToString("#.00"));
        }
    }
}