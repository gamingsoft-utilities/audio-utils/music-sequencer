﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace GamingSoft.AudioUtils.MusicSequencer 
{
    [CustomPropertyDrawer(typeof(MusicPhase), true)]
    internal class MusicPhaseDrawer : PropertyDrawer
    {
        MusicPhase GetMusicPhaseFromProperty(SerializedProperty property)
        {
            var obj = fieldInfo.GetValue(property.serializedObject.targetObject);
            MusicPhase myDataClass = obj as MusicPhase;
            if(obj.GetType().IsArray)
            {
                var index = Convert.ToInt32(new string(property.propertyPath.Where(c => char.IsDigit(c)).ToArray()));
                myDataClass = ((MusicPhase[])obj)[index];
            }
            return myDataClass;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            float baseHeight = EditorGUIUtility.singleLineHeight;
            
            EditorGUI.PropertyField(position, property, true);
            if(property.isExpanded)
            {
                MusicPhase myDataClass = GetMusicPhaseFromProperty(property);

                // Draw info labels
                Rect labelPos = position;
                
                EditorGUI.indentLevel++;
                
                labelPos.y += GetPropertyHeight(property, label) - (baseHeight * 4.5f); // four extra fields + 1 line

                // draw line
                EditorGUI.DrawRect(new Rect(labelPos.x, labelPos.y, labelPos.width, 1), Color.black);
                
                labelPos.y += baseHeight*0.5f; // move down one line

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.AppendFormat("Length: {0:N2}s - Is Playing: {1}\n", myDataClass.Length, myDataClass.IsPlaying);
                sb.AppendFormat("Beats: {0:N0} - Current: {1:N0}\n", myDataClass.Beats, myDataClass.CurrentBeat);
                sb.AppendFormat("Bars: {0:N2} - Current: {1:N2}\n", myDataClass.Bars, myDataClass.CurrentBar);
                sb.AppendFormat("Loop Length: {0:N2}s", myDataClass.LoopLength);

                EditorGUI.LabelField(labelPos, sb.ToString(), GUIStyle.none);
                
                labelPos.y += baseHeight * 3.5f; // move down to the end of the label
                
                // draw end line
                EditorGUI.DrawRect(new Rect(labelPos.x, labelPos.y, labelPos.width, 1), Color.black);

                EditorGUI.indentLevel--;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            float totalHeight = EditorGUI.GetPropertyHeight (property, label, true) + EditorGUIUtility.standardVerticalSpacing;
            
            // account for 4 extra fields
            if(property.isExpanded)
            {   // add 4 new fields + 1 line
                totalHeight += EditorGUIUtility.singleLineHeight * 5;
            }
            
            return totalHeight;
        }
    }
}