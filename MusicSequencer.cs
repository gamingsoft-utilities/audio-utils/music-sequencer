﻿using System;
using UnityEngine;
using UnityEngine.Audio;

namespace GamingSoft.AudioUtils.MusicSequencer
{
    public class MusicSequencer : MonoBehaviour
    {
        void OnValidate()
        {
            if (phases != null)
            {
                for (int i = 0; i < phases.Length; ++i)
                {
                    phases[i].OnValidate();
                }
            }
        }

        public int CurrentPhaseIndex { get; private set; }
        MusicPhase CurrentPhase { get { return CurrentPhaseIndex < phases.Length ? phases[CurrentPhaseIndex] : null; } }

        [SerializeField] AudioMixerGroup defaultMixerGroup;
        [SerializeField] MusicPhase[] phases;
        [SerializeField] bool autoStart;
        AudioSourcesPool _sourcesPool;

        private void Awake()
        {
            if (phases == null) { return; } // do nothing
            _sourcesPool = new AudioSourcesPool(gameObject);

            for (int i = 0; i < this.phases.Length; ++i)
            {   // Create audioSources and subscribe to transition events
                phases[i].Init(defaultMixerGroup,_sourcesPool);
                phases[i].OnAboutToEnd += MusicSequencer_OnAboutToEnd;
            }
        }

        private void OnDestroy()
        {
            if (phases == null) { return; } // do nothing

            for (int i = 0; i < this.phases.Length; ++i)
            {   // Unsubscribe from transition events
                phases[i].OnAboutToEnd -= MusicSequencer_OnAboutToEnd;
            }
        }

        private void MusicSequencer_OnAboutToEnd(MusicPhase phase)
        {
            if (phase.endBehaviour == EndBehaviour.ToNextOne)
            {   // Schedule next one
                ToNextPhase();
            }
        }

        private void Start()
        {
            if (!Application.isPlaying) { return; }
            // start only if auto start is set to true
            if(autoStart) { phases[CurrentPhaseIndex].Play(); }
        }

        private void FixedUpdate()
        {
            if (phases == null) { return; }
            foreach (var item in phases)
            {
                item.Update();
            }
        }

        [ContextMenu("To Next Phase")]
        public void ToNextPhase()
        {
            if (!IsInCurrentPhase) {
                Debug.LogWarningFormat("Transition to current phase is pending. Wait and try in a few seconds");
                return;
            }

            if (CurrentPhaseIndex >= phases.Length - 1) { return; }
            TransitionPhase(CurrentPhaseIndex, CurrentPhaseIndex+1);
        }

        [ContextMenu("To Previous Phase")]
        public void ToPreviousPhase()
        {
            if (!IsInCurrentPhase) {
                Debug.LogWarningFormat("Transition to current phase is pending. Wait and try in a few seconds");
                return;
            }

            if (CurrentPhaseIndex <= 0) { return; }
            TransitionPhase(CurrentPhaseIndex, CurrentPhaseIndex-1);
        }

        public void TransitionPhase(int currentPhaseIndex, int nextPhaseIndex)
        {
            if (!Application.isPlaying) { return; } // Don't trigger a phase change if we are not within playing mode

            MusicPhase currentPhase = phases[currentPhaseIndex];
            MusicPhase nextPhase = phases[nextPhaseIndex];

            if (!currentPhase.IsPlaying) 
            {
                Debug.LogWarningFormat("Unable to Transition to Phase [{0}] because current Phase [{1}] has not started yet",nextPhaseIndex,currentPhaseIndex);
                return;
            }

            CurrentPhaseIndex = nextPhaseIndex;           

            switch (currentPhase.transitionType)
            {
                case TransitionType.CuePoint: // schedules play to next cue point time
                    currentPhase.StopSchedule(currentPhase.NextCuePointTime, currentPhase.fadeTime);
                    nextPhase.PlayScheduled(currentPhase.NextCuePointTime);
                    nextPhase.SetVolumeFade(0.1f, 1f, 1f, currentPhase.NextCuePointTime);
                    break;
                case TransitionType.Bars:
                    currentPhase.StopSchedule(currentPhase.NextDynamicCuePointTime, currentPhase.fadeTime);
                    nextPhase.PlayScheduled(currentPhase.NextDynamicCuePointTime);
                    nextPhase.SetVolumeFade(0.1f, 1f, 1f, currentPhase.NextDynamicCuePointTime);
                    break;
                case TransitionType.Fade: // do a crossFade between current and next
                    currentPhase.SetVolumeFade(nextPhase.fadeTime, 1f, 0f); // start fade now, using one second as transition time
                    nextPhase.PlayScheduled(AudioSettings.dspTime);
                    nextPhase.SetVolumeFade(nextPhase.fadeTime, 0f, 1f);
                    break;
                case TransitionType.None: // stop current and play next immediately
                    currentPhase.Stop();
                    nextPhase.Play();
                    nextPhase.SetVolumeFade(0.1f, 1f, 1f); // Set volume to max value, just in case
                    break;
            }
        }

        public bool IsInCurrentPhase { get  { return CurrentPhase.IsPlaying; } }
        
        [ContextMenu("Play")] public void Play() { CurrentPhase.Play(); }
        [ContextMenu("Pause")] public void Pause() { CurrentPhase.Pause(); }
        [ContextMenu("Resume")] public void Resume() { CurrentPhase.Resume(); }
        [ContextMenu("Stop")] public void Stop() { CurrentPhase.Stop(); }
        [ContextMenu("Stop All!")] public void StopAll()
        {
            if (phases == null) { return; }
            for (int i = 0; i < phases.Length; ++i) 
            {
                phases [i].StopAll();
            }
        }
    }

}