﻿using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Audio;

namespace GamingSoft.AudioUtils.MusicSequencer
{
    public enum BarType { FourByFour, SixByEight }
    public enum EndBehaviour { Stop, Loop, ToNextOne }
    public enum TransitionType { None, Fade, CuePoint, Bars }

    [Serializable]
    public class MusicPhase
    {
        public string name;

        public MusicPhaseAsset phaseAsset;

        public AudioClip AudioClip => phaseAsset != null ? phaseAsset.audioClip : null;
        public int Bpm => phaseAsset != null ? phaseAsset.bpm : 0;
        public BarType BarType => phaseAsset != null ? phaseAsset.barType : BarType.FourByFour;
        public Vector2 LoopStartEndBar => phaseAsset != null ? phaseAsset.loopStartEndBar : Vector2.zero;

        public EndBehaviour endBehaviour;
        public TransitionType transitionType;
        [Range(0f,10f)] public float fadeTime;
        [Tooltip("Start of a cue point. Bar number")] public float[] cuePoints;
        [Tooltip("bar count to calculate next cue point dynamically")][Min(1)] public int cuePointBars;

        public event Action<MusicPhase> OnAboutToEnd = (phase) => { };
        private bool _onAboutToEndTriggered;
        private List<AudioSource> _markedForRelease = new List<AudioSource>();

        // for stats calculation

        private bool _isValid; // true once CalculateStats has run once
        private bool _started; // to update phase start time the first time it's played (ie, when level is just loaded)
        private double _startTime;
        private double _nextPlayTime;

        public double Bars => phaseAsset != null ? phaseAsset.Bars : 0;
        public double Beats => phaseAsset != null ? phaseAsset.Beats : 0;
        public double CurrentBeat { get; private set; }
        public double CurrentBar { get; private set; }

        public double SecondsPerBeat => phaseAsset != null ? phaseAsset.SecondsPerBeat : 0;
        public double SecondsPerBar => phaseAsset != null ? phaseAsset.SecondsPerBar : 0;

        public double Length => phaseAsset != null ? phaseAsset.Length : 0;
        public double LoopLength => phaseAsset!= null ? phaseAsset.LoopLength : 0;

        public double ElapsedTime => _startTime != 0 ? AudioSettings.dspTime - _startTime : 0;

        public bool IsPlaying => _audioSources != null 
                                 && _audioSources.GetCurent() != null 
                                 && _audioSources.GetCurent().isPlaying 
                                 && _audioSources.GetCurent().time > 0;
        public bool IsAboutToEnd => AudioSettings.dspTime + 1 > _nextPlayTime;

        public double SecondsToSectionEnd
        {
            get
            {
                double toReturn = 0;

                switch (endBehaviour)
                {
                    case EndBehaviour.Stop:
                        return (_startTime + LoopLength) - AudioSettings.dspTime;
                    case EndBehaviour.Loop:
                        return double.PositiveInfinity;
                    case EndBehaviour.ToNextOne:
                        return NextCuePointTime;
                }

                return toReturn;
            }
        }

        private AudioSourcesCollection _audioSources;

        public void Init(AudioMixerGroup defaultMixerGroup, AudioSourcesPool audioSourcesPool)
        {
            _audioSources = new AudioSourcesCollection();
            _audioSources.Init(audioSourcesPool,defaultMixerGroup);
        }

        public void Play()
        {
            PlayScheduled(AudioSettings.dspTime + 1d);
        }

        public void PlayScheduled(double startTime)
        {
            if(!Application.isPlaying)
            {
                Debug.LogError("Playing clips is only supported at play mode!");
                return;
            }

            this._startTime = startTime;

            CalculateMusicStats();

            AudioSource currentAudioSource = _audioSources.GetNextFree();
            currentAudioSource.loop = false;
            currentAudioSource.playOnAwake = false;
            currentAudioSource.clip = AudioClip;

            currentAudioSource.PlayScheduled(startTime);
            _nextPlayTime = startTime + LoopLength;

            _onAboutToEndTriggered = false;
        }

        void CalculateMusicStats()
        {
            if (phaseAsset != null)
            {
                name = phaseAsset.name;
                phaseAsset.CalculateStats();
                _isValid = true;
            }

            _isValid = false;
        }

        public void Update()
        {
            if (phaseAsset == null || !phaseAsset.IsValid) { return; } // skip update is phase asset is null

            if (!Application.isPlaying) { return; } // run this only on play time
            if (!IsPlaying) { return; } // skip if no audio is playing

            if (!_started)
            {   // refresh dsp time (only needed the first time a phase is played)                
                double realTimeStart = AudioSettings.dspTime - _audioSources.GetCurent().time;
                _startTime = realTimeStart;
                _started = true;
            }

            CalculateMusicStats();
            CurrentBeat = ElapsedTime / phaseAsset.SecondsPerBeat;
            CurrentBar = ElapsedTime / phaseAsset.SecondsPerBar;            

            if ( IsAboutToEnd && !IsMusicFadeRunning && endBehaviour == EndBehaviour.Loop) // there is no fade out in process
            {
                // check if we need to use another audioSource
                if(Mathf.Approximately((float)LoopLength,(float)Length))
                {
                    _audioSources.GetCurent().loop = true;
                    RecalculateNextPlayTime();
                }
                else
                {
                    // schedule release of current audioSource
                    var audioSourceToBeRemoved = _audioSources.GetCurent();
                    audioSourceToBeRemoved.loop = false;
                    _markedForRelease.Add(audioSourceToBeRemoved);

                    // schedule new loop on another audioSource
                    AudioSource newSource = _audioSources.GetNextFree();
                    newSource.clip = AudioClip;
                    newSource.loop = false;
                    newSource.playOnAwake = false;
                    newSource.time = 0;
                    _nextPlayTime = Math.Max(_nextPlayTime, AudioSettings.dspTime); // don't schedule playback in the past
                    _startTime = _nextPlayTime;
                    newSource.PlayScheduled(_startTime);
                    _nextPlayTime += LoopLength;
                }
            }
            if (endBehaviour == EndBehaviour.ToNextOne && IsAboutToEnd && !_onAboutToEndTriggered)
            {
                OnAboutToEnd(this);
                _onAboutToEndTriggered = true;
            }

            ProcessAudioFade();
            ProcessAudioSourcesRelease();
        }

        public void OnValidate()
        {
            if (!_isValid) { return; }

            if (phaseAsset.loopStartEndBar.x < 1) { phaseAsset.loopStartEndBar.x = 1; }
            if (phaseAsset.loopStartEndBar.y > Bars) { phaseAsset.loopStartEndBar.y = (float)Bars; }
        }

        private bool IsMusicFadeRunning => _fadeLength > 0 && AudioSettings.dspTime >= _fadeStartTime && AudioSettings.dspTime < _fadeEndTime;

        private bool IsMusicFadeFinished => _fadeLength > 0 && AudioSettings.dspTime > _fadeEndTime;

        private void ProcessAudioFade()
        {
            if (!Application.isPlaying) { return; } // ignore fade processing
            if (!IsMusicFadeRunning && !IsMusicFadeFinished) { return; } // ignore fade processing

            if (!IsPlaying)
            {
                Debug.LogWarning("AudioSources is null or there are no active audio sources. Unable to process audio fade");
                return;
            }

            List<AudioSource> allAudioSources = _audioSources.GetAllActive();

            // process audio fade
            double currentTime = AudioSettings.dspTime;

            if ( _fadeLength > 0 && currentTime >= _fadeStartTime && currentTime < _fadeEndTime)
            {
                float elapsedTime = _fadeLength - (float)(_fadeEndTime - currentTime);
                float elapsedPercent = (elapsedTime / _fadeLength);
                for (int i = 0; i < allAudioSources.Count; ++i)
                {
                    float volumeToSet = Mathf.Lerp(_fadeStartValue, _fadeEndValue, elapsedPercent);
                    allAudioSources[i].volume = volumeToSet;
                }
            }
            else if (_fadeLength > 0 && currentTime > _fadeEndTime)
            {   // conclude fade out and set final values
                for (int i = 0; i < allAudioSources.Count; ++i)
                {
                    allAudioSources[i].volume = _fadeEndValue;
                }

                if (_fadeEndValue <= 0) // stop audio clip
                {
                    Stop();
                }
                _fadeEndTime = _fadeStartValue = _fadeEndValue = 0;
                _fadeLength = 0;
            }
        }

        void ProcessAudioSourcesRelease()
        {
            if (_markedForRelease.Count == 0) { return; } // there is nothing to do

            _markedForRelease.RemoveAll(w => w.loop); // skip audio sources who are marked for loop

            for (int i = 0; i < _markedForRelease.Count; ++i)
            {
                if (_markedForRelease[i].isPlaying) { continue; } // if playing, check next one
                // release and remove from marked for release
                _audioSources.Release(_markedForRelease[i]);
                _markedForRelease.Remove(_markedForRelease[i]);
            }
        }

        void RecalculateNextPlayTime()
        {
            if (_audioSources.GetCurent() == null) { return; }
            float currentPlayedTime = _audioSources.GetCurent().time;
            _startTime = AudioSettings.dspTime - currentPlayedTime;
            _nextPlayTime = _startTime + LoopLength;
            _onAboutToEndTriggered = false;
        }

        public void Pause()
        {
            if (_audioSources.GetCurent() == null) { return; }
            _audioSources.GetCurent().Pause();
        }

        public void Resume()
        {
            if (_audioSources.GetCurent() == null) { return; }
            if (_audioSources.GetCurent().time > 0)
            { // was paused before
                _audioSources.GetCurent().UnPause();
            }
            else
            { // was stopped or it was never played
                _audioSources.GetCurent().Play();
            }
            RecalculateNextPlayTime();
        }

        public void Stop()
        {
            if (_audioSources.GetCurent() == null) { return; }
            _audioSources.GetCurent().Stop();
            _nextPlayTime = double.PositiveInfinity; // this phase should now be stopped. ignore next play time
            _audioSources.Release(_audioSources.GetCurent());
        }

        /// <summary>
        /// Stops all audio sources related to this phase
        /// </summary>
        public void StopAll()
        {
            List<AudioSource> allActiveSources = _audioSources.GetAllActive ();
            for(int i = 0; i < allActiveSources.Count; ++i)
            {
                allActiveSources [i].Stop ();
                _audioSources.Release(allActiveSources[i]);
            }
            _nextPlayTime = double.PositiveInfinity; // this phase will be stoped. ignore next play time
        }

        /// <summary>
        /// stops current playing music at a given time
        /// </summary>
        /// <param name="time">When the music must stop (absolute time)</param>
        /// <param name="fadeLength">greater than 0 if you want to stop the music with a volume fade</param>
        public void StopSchedule(double time, float fadeLength = 0)
        {
            _nextPlayTime = double.PositiveInfinity; // this phase will be stopped. ignore next play time
            fadeLength = Mathf.Max(fadeLength, 0.1f);
            SetVolumeFade(fadeLength, 1f, 0f, time);
        }

        private float _fadeLength;
        private double _fadeStartTime;
        private double _fadeEndTime;
        private float _fadeStartValue;
        private float _fadeEndValue;

        public void SetVolumeFade(float fadeLength, float startVolumeValue, float endVolumeValue, double startTime = double.NegativeInfinity)
        {
            // Set initial fade value
            if (_audioSources == null || _audioSources.GetCurent() == null) { return; } // there is no clip set
            _audioSources.GetCurent().volume = startVolumeValue;

            _fadeLength = fadeLength;
            _fadeStartTime = !double.IsNegativeInfinity(startTime) ? startTime : AudioSettings.dspTime;
            _fadeEndTime = _fadeStartTime + fadeLength;
            _fadeStartValue = startVolumeValue;
            _fadeEndValue = endVolumeValue;
        }

        /// <summary>
        /// Cue Points are represented in bars where, decimals represents beats within a bar
        /// ie: 1.5 means 1 bar and 2 beats, if time is set using 4 by 4
        /// </summary>
        private float NextCuePoint => GetNextCuePoint(cuePoints);

        private float GetNextCuePoint(int bars)
        {
            var totalBars = LoopStartEndBar.y;
            var fakeCuePoints = new List<float>();
            for (int i = 0; i < totalBars; i = i + bars)
            {
                fakeCuePoints.Add(i);
            }

            return GetNextCuePoint(fakeCuePoints);
        }

        private float GetNextCuePoint(IList<float> cuePoints)
        {
            float toReturn = LoopStartEndBar.y; // set last bar as default cue point
            for (int i = cuePoints.Count -1; i >= 0; --i)
            {
                float item = cuePoints[i];
                if (item > CurrentBar && item < toReturn)
                {
                    toReturn = item;
                }
            }

            return toReturn;
        }

        /// <summary>
        /// Gets amount of time (in seconds) for the next cue point
        /// </summary>
        public double NextCuePointTime => _startTime + ( SecondsPerBar * NextCuePoint);

        /// <summary>
        /// Gets amount of time (in seconds) for next dynamic cue point (using bars split)
        /// </summary>
        public double NextDynamicCuePointTime => _startTime + (SecondsPerBar * GetNextCuePoint(cuePointBars));
    }
}

